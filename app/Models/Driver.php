<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Invoice;
use App\Models\Vehicle;

class Driver extends  Authenticatable
{
    use HasFactory;
    protected $table = 'tbl_drivers';
    protected $fillable = ['drvid', 'username', 'password', 'fname', 'mname', 'lname', 'gender', 'phone', 'addr', 'city', 'modal', 'brand', 'updated_on'];
    public $timestamps = false;
    protected $primaryKey = 'drvid';


    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'drvid', 'schedule_for');
    }

    public function vehicles()
   {
      return $this->belongsToMany(Vehicle::class);
   }
}
