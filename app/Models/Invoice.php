<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Driver;

class Invoice extends Model
{
    use HasFactory;
    protected $table ='tbl_invoices';

    public function driver()
   {
      return $this->belongsTo(Driver::class,'schedule_for', 'drvid');
   }
}
