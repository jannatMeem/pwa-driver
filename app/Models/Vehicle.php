<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Driver;


class Vehicle extends Model
{
    use HasFactory;

    protected $table = 'tbl_vehicles';
    protected $primaryKey = 'tvhid ';
    protected $fillable = ['tvhid', 'drvid', 'vehicle', 'created_on', 'updated_on'];
    public $timestamps = false;
    public function drivers()
    {
        return $this->belongsTo(Driver::class, 'drvid', 'drvid');
    }
}
