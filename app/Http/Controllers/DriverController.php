<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Session;
use App\Models\Driver;
use App\Models\Invoice;
use App\Models\City;
use App\Models\Vehicle;

class DriverController extends Controller
{


    //register
    function register(Request $req)
    {
        //GET HEADER ATTRIBUTE
        $city = City::orderBy('label', 'ASC')->get();
        // dd($city);
        return view('Driver/register', ['city' => $city,]);
    }


    function driverCreate(Request $req)
    {
        //FORM VALIDATION
        $req->validate([
            'username' => 'required|unique:tbl_drivers,username|max:12|min:3',
            'password' => 'required|max:12|min:8',
            'phone' => 'required|max:12|min:5',
            'fname' => 'required|max:45|min:3',
            'mname' => 'required|max:45|min:3',
            'lname' => 'required|max:45|min:3',
            'address' => 'required|max:100|min:10',
            'city' => 'required',

            'modal' => 'required|max:4|min:4',
            'brand' => 'required|max:45|min:3',
            'referredby' => 'required|max:45|min:3',

            'profileImage' => 'required|mimes:jpeg,jpg,png',
            'drvlicense' => 'required|mimes:jpeg,jpg,png',
            'voreceipt' => 'required|mimes:jpeg,jpg,png',
            'vcregistration' => 'required|mimes:jpeg,jpg,png',
            'vehicles' => 'required',
        ]);

        $cityname = City::find($req->city)->label;
        $driver = new Driver;
        $driver->username = $req->username;
        $driver->password = Hash::make($req->password);
        $driver->phone = $req->phone;
        $driver->fname = $req->fname;
        $driver->mname = $req->mname;
        $driver->lname = $req->lname;
        $driver->gender = $req->gender;
        $driver->addr = $req->address;
        $driver->cid = $req->city;

        $driver->city = $cityname;
        $driver->modal = $req->modal;
        $driver->brand = $req->brand;

        $driver->referredby = $req->referredby;

        $folder = "img/driver/";

        //profile image
        $pimage = $req->file('profileImage');
        $pimage_new_name = time() . '-' . $pimage->getClientOriginalName();
        $pimage->move($folder, $pimage_new_name);
        $pimageUrl = $folder . $pimage_new_name;

        $driver->profile_img = $pimageUrl;

        //driver licence image
        $dlimage = $req->file('drvlicense');
        $dlimage_new_name = time() . '-' . $dlimage->getClientOriginalName();
        $dlimage->move($folder, $dlimage_new_name);
        $dlimageUrl = $folder . $dlimage_new_name;

        $driver->drvlicense = $dlimageUrl;

        //Vehicle Reciept
        $vrimage = $req->file('voreceipt');
        $vrimage_new_name = time() . '-' . $vrimage->getClientOriginalName();
        $vrimage->move($folder, $vrimage_new_name);
        $vrimageUrl = $folder . $vrimage_new_name;

        $driver->voreceipt = $vrimageUrl;

        //Vehicle Reciept image
        $viimage = $req->file('vcregistration');
        $viimage_new_name = time() . '-' . $viimage->getClientOriginalName();
        $viimage->move($folder, $viimage_new_name);
        $viimageUrl = $folder . $viimage_new_name;

        $driver->vcregistration = $viimageUrl;


        if ($driver->save()) {

            $driverId = $driver->drvid;

            if (count($req->vehicles) > 0) {
                foreach ($req->vehicles as $vehicle) {
                    //ADDING PIVOT RELATION
                    $vehicle = Vehicle::create(['drvid' => $driverId, 'vehicle' => $vehicle, 'created_on' => date('Y-m-d H:i:s'), 'updated_on' => date('Y-m-d H:i:s')]);
                }
            }

            session()->flash('message', 'You account has been created Successfully');

            return  redirect('/login');
        } else {

            session()->flash('warningmsg', 'Something went wrong! Please try again!');
            return  redirect('register');
        }
    }



    function updateLocation(Request $req, $id)
    {

        $alreadyAssigned = DB::table('tbl_delivery_track')
            ->where('driver_id', $id)
            ->get();

        if ($alreadyAssigned->count() == 0) {
            DB::table('tbl_delivery_track')->insert(array('driver_id' => $id, 'track_lat' =>  $req->Lat, 'track_lng' =>  $req->Long, 'track_time' =>  date('Y-m-d H:i:s'),));
        } else {
            DB::table('tbl_delivery_track')->where('driver_id', $id)->update(array('track_lat' =>  $req->Lat, 'track_lng' =>  $req->Long, 'track_time' =>  date('Y-m-d H:i:s'),));
        }
        return true;
    }


    function login(Request $req)
    {
        return view('Driver/login');
    }


    function driverAuthenticateLogin(Request $request)
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        //USING WRITER GURD
        if (Auth::guard('driver')->attempt($credentials)) {

            return redirect('/dashboard');
        } else {
            session()->flash('credentialserror', 'The provided credentials do not match our records!');
            return redirect()->route('driver.login');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('driver.login');
    }

    //DRIVER DASHBOARD
    function dashboard(Request $req)
    {

        $driverId = Auth::guard('driver')->user()->drvid;
        // $driver = Driver::where('drvid', $driverId)->first();
        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.is_scheduled', 0)
            ->where('tbl_delivery.status', 'pending')
            ->orWhere('tbl_delivery.status', 'cancelled')
            ->get();


        $acceptedOrders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'accepted')
            ->get()
            ->count();

        $deliveredOrders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'delivered')
            ->get()
            ->count();
        $cancelledOrders = DB::table('tbl_delivery_cancelled')
            ->select('tbl_delivery.*', 'tbl_delivery_cancelled.*')
            ->join('tbl_delivery', 'tbl_delivery_cancelled.delivery_id', '=', 'tbl_delivery.id')
            ->where('tbl_delivery_cancelled.driver_id', $driverId)
            ->get()
            ->count();

        $data = array(
            'total_available_orders' => $orders->count(),
            'total_accepted_orders' => $acceptedOrders,
            'total_delivered_orders' => $deliveredOrders,
            'total_cancelled_orders' => $cancelledOrders,
        );

        return view('Driver/dashboard', ['data' => $data, 'orders' => $orders,]);
    }

    function deliveryPool(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;


        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.is_scheduled', 0)
            ->where('tbl_delivery.status', 'pending')
            ->orWhere('tbl_delivery.status', 'cancelled')
            ->get();
        $orderCount = $orders->count();

        return view('Driver/deliveryPool', ['orders' => $orders, 'orderCount' => $orderCount]);
    }

    //DRIVER DASHBOARD
    function pendingOrders(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;

        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'pending')
            ->get();
        $orderCount = $orders->count();
        return view('Driver/pendingOrders', ['orders' => $orders, 'orderCount' => $orderCount]);
    }

    function acceptedOrders(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;

        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'accepted')
            ->get();
        $orderCount = $orders->count();
        return view('Driver/acceptedOrders', ['orders' => $orders, 'orderCount' => $orderCount]);
    }


    function pickupOrders(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;

        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'picked')
            ->get();
        $orderCount = $orders->count();
        return view('Driver/pickupOrders', ['orders' => $orders, 'orderCount' => $orderCount]);
    }



    //DRIVER DASHBOARD
    function ongoingOrder(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;


        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'ongoing')
            ->get();

        $orderCount = $orders->count();
        return view('Driver/ongoingOrder', ['orders' => $orders, 'orderCount' => $orderCount]);
    }

    function completedOrders(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;


        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.status', 'delivered')
            ->get();
        $orderCount = $orders->count();
        return view('Driver/completedOrders', ['orders' => $orders, 'orderCount' => $orderCount]);
    }

    function cancelledOrders(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;


        $orders = DB::table('tbl_delivery_cancelled')
            ->select('tbl_delivery.*', 'tbl_delivery_cancelled.*')
            ->join('tbl_delivery', 'tbl_delivery_cancelled.delivery_id', '=', 'tbl_delivery.id')
            ->where('tbl_delivery_cancelled.driver_id', $driverId)
            ->get();

        $orderCount = $orders->count();
        return view('Driver/cancelledOrders', ['orders' => $orders, 'orderCount' => $orderCount]);
    }

    function getNotification()
    {
        $driverId = Auth::guard('driver')->user()->drvid;


        $notifications = DB::table('tbl_notifications')
            ->select('tbl_notifications.*')
            ->orderBy('nid', 'desc')
            ->limit(3)
            ->get();


        //     // SELECT * FROM comments ORDER BY comment_id DESC LIMIT 5
        // return json_encode($notification);

        // if(isset($_POST['view'])){
        // if($_POST["view"] != '')
        // {
        //    $update_query = "UPDATE comments SET comment_status = 1 WHERE comment_status=0";
        //    mysqli_query($con, $update_query);
        // }
        $output = '';
        if ($notifications->count() > 0) {

            $output = $notifications;
        } else {
            $output = 'No';
        }

        $notificationsUnseen = DB::table('tbl_notifications')
            ->select('tbl_notifications.*')
            ->where('status', 0)
            ->get();
        $count = $notificationsUnseen->count();

        $data = array(
            'notification' => $output,
            'unseen_notification'  => $count
        );
        return json_encode($data);
    }


    function profileSettings(Request $req)
    {
        //GET HEADER ATTRIBUTE
        $city = City::orderBy('label', 'ASC')->get();

        return view('Driver/driverSettings', ['city' => $city,]);
    }

    function editProfile(Request $req)
    {
        $req->validate([
            'firstName' => 'required|min:3|max:45',
            'middleName' => 'required|min:3|max:45',
            'lastName' => 'required|min:3|max:45',
            'gender' => 'required',
            'phone' => 'required|min:5|max:12',
            'address' => 'required',
            'city' => 'required',
            'modal' => 'required|min:4|max:4',
            'brand' => 'required|min:3|max:45'

        ]);

        $driverId = Auth::guard('driver')->user()->drvid;
        $driver = Driver::find($driverId);

        $folder = "img/driver/";

        //profile image
        if ($req->file('profileImage') != null) {

            $req->validate([
                'profileImage' => 'required|mimes:jpeg,jpg,png',
            ]);
            $path = Auth::guard('driver')->user()->profile_img;

            //CHECK IF OLD IMAGE EXIST
            if ($path != 'img/driver/defaultUser.jpg' && File::exists($path)) {

                //DELETE IF OLD IMAGE EXIST
                File::delete($path);
            }

            $pimage = $req->file('profileImage');
            $pimage_new_name = time() . '-' . $pimage->getClientOriginalName();
            $pimage->move($folder, $pimage_new_name);
            $pimageUrl = $folder . $pimage_new_name;

            $driver->profile_img = $pimageUrl;
        }

        //driver licence image
        if ($req->file('drvlicense') != null) {

            $req->validate([
                'drvlicense' => 'required|mimes:jpeg,jpg,png',
            ]);

            $drpath = Auth::guard('driver')->user()->drvlicense;
            //CHECK IF OLD IMAGE EXIST
            if (File::exists($drpath)) {
                //DELETE IF OLD IMAGE EXIST
                File::delete($drpath);
            }


            $dlimage = $req->file('drvlicense');
            $dlimage_new_name = time() . '-' . $dlimage->getClientOriginalName();
            $dlimage->move($folder, $dlimage_new_name);
            $dlimageUrl = $folder . $dlimage_new_name;

            $driver->drvlicense = $dlimageUrl;
        }

        //Vehicle Reciept
        if ($req->file('voreceipt') != null) {

            $req->validate([
                'voreceipt' => 'required|mimes:jpeg,jpg,png',
            ]);

            $vrpath = Auth::guard('driver')->user()->voreceipt;
            //CHECK IF OLD IMAGE EXIST
            if (File::exists($vrpath)) {
                //DELETE IF OLD IMAGE EXIST
                File::delete($vrpath);
            }


            $vrimage = $req->file('voreceipt');
            $vrimage_new_name = time() . '-' . $vrimage->getClientOriginalName();
            $vrimage->move($folder, $vrimage_new_name);
            $vrimageUrl = $folder . $vrimage_new_name;

            $driver->voreceipt = $vrimageUrl;
        }

        //Vehicle Reciept image
        if ($req->file('vcregistration') != null) {

            $req->validate([
                'vcregistration' => 'required|mimes:jpeg,jpg,png',
            ]);

            $vripath = Auth::guard('driver')->user()->vcregistration;
            //CHECK IF OLD IMAGE EXIST
            if (File::exists($vripath)) {
                //DELETE IF OLD IMAGE EXIST
                File::delete($vripath);
            }

            $viimage = $req->file('vcregistration');
            $viimage_new_name = time() . '-' . $viimage->getClientOriginalName();
            $viimage->move($folder, $viimage_new_name);
            $viimageUrl = $folder . $viimage_new_name;

            $driver->vcregistration = $viimageUrl;
        }

        if ($req->password != null) {

            $req->validate([
                'password' => 'required|max:12|min:8',
            ]);
            $driver->password = Hash::make($req->password);
        }


        $cityname = City::find($req->city)->label;

        $driver->fname = $req->firstName;
        $driver->mname = $req->middleName;
        $driver->lname = $req->lastName;
        $driver->gender = $req->gender;
        $driver->phone = $req->phone;
        $driver->addr = $req->address;

        $driver->cid = $req->city;
        $driver->city = $cityname;

        $driver->modal = $req->modal;
        $driver->brand = $req->brand;
        $driver->updated_on = date('Y-m-d H:i:s');


        if ($driver->update()) {

            session()->flash('message', 'Your profile has been updated!');
            return  redirect('/profile');
        } else {
            session()->flash('warningmsg', 'Could not update your profile! Please try again! ');
            return  redirect('/profile');
        }
    }

    function pictureRemove(Request $req)
    {


        $driverId = Auth::guard('driver')->user()->drvid;
        $driver = Driver::find($driverId);
        $profileImage = Auth::guard('driver')->user()->profile_img;
        $folder = "img/driver/";

        //profile image
        if ($driver->profile_img != 'img/driver/defaultUser.jpg') {

            $path = $profileImage;

            //CHECK IF OLD IMAGE EXIST
            if (File::exists($path)) {

                //DELETE IF OLD IMAGE EXIST
                File::delete($path);
            }

            $driver->profile_img = 'img/driver/defaultUser.jpg';
            if ($driver->update()) {

                session()->flash('message', 'Your profile picture has been removed!');
                return  redirect('/profile');
            } else {
                session()->flash('warningmsg', 'Could not update your profile! Please try again! ');
                return  redirect('/profile');
            }
        } else {

            session()->flash('warningmsg', 'You dosent have a valid profile picture!');
            return  redirect('/profile');
        }
    }
}
