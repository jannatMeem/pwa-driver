<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Session;
use App\Models\Driver;
use App\Models\Invoice;
use App\Models\Delivery;


class DeliveryController extends Controller
{

    function getDelivery($id)
    {
        $delivery = Delivery::find($id);
        return json_encode($delivery);
    }

    function acceptDelivery(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;

        $deliveryId = $req->deliveryId;
        $invId = $req->invId;


        DB::table('tbl_delivery')->where('id', $deliveryId)->update(array('driver_id' => $driverId, 'status' => 'accepted'));

        DB::table('tbl_invoices')->where('invoice_id', $invId)->update(array('order_status' => 'accepted', 'delivery_status' => 1, 'updated_at' => date('Y-m-d H:i:s')));

        $alreadyAssigned = DB::table('tbl_assigned_orders')
            ->where('oid', $invId)
            ->get();
        if ($alreadyAssigned->count() == 0) {
            DB::table('tbl_assigned_orders')->insert(array('drvid' => $driverId, 'oid' => $invId, 'drop_location' => $req->drop_location_add,  'delivery_date' => date('Y-m-d H:i:s')));
        } else {
            DB::table('tbl_assigned_orders')->where('oid', $invId)->update(array('delivery_date' => date('Y-m-d H:i:s')));
        }

        session()->flash('message', 'You have accepted a Delivery Order');
        return  redirect('/acceptedOrders');
    }

    function deliveryStatusChange(Request $req)
    {
        $driverId = Auth::guard('driver')->user()->drvid;

        $deliveryId = $req->deliveryId;
        $invId = $req->invId;

        $status = $req->status;
        $deliveryId1 = $req->deliveryId1;
        $invId1 = $req->invId1;
        $recepient = $req->recepientName;

        if ($status == 'picked') {
            DB::table('tbl_delivery')->where('id', $deliveryId)->update(array('status' => $status));

            DB::table('tbl_invoices')->where('invoice_id', $invId)->update(array('order_status' => $status, 'updated_at' => date('Y-m-d H:i:s')));

            session()->flash('message', 'Delivery Picked up!');
            return redirect('/pickupOrders');
        } else {

            $this->validate($req, [
                'signature' => 'required|mimes:jpeg,jpg,png',
            ]);
            //IMAGE PROCESSING

            $image = $req->file('signature');
            $image_name = $image->getClientOriginalName();
            $image_new_name = time() . '-' . $image_name;


            $folder = "img/driver/signature/";
            $image->move($folder, $image_new_name);
            $imageUrl = $folder . $image_new_name;

            DB::table('tbl_delivery')->where('id', $deliveryId1)->update(array('status' => $status, 'recepientName' => $recepient, 'signature' => $imageUrl, 'delivery_date' => date('Y-m-d H:i:s')));

            DB::table('tbl_invoices')->where('invoice_id', $invId1)->update(array('deliver_by' => $driverId, 'delivered_at' => date('Y-m-d H:i:s'), 'order_status' => $status, 'updated_at' => date('Y-m-d H:i:s')));

            DB::table('tbl_assigned_orders')->where('oid', $invId1)->update(array('drvid' => $driverId, 'is_completed' => 1, 'delivery_date' => date('Y-m-d H:i:s')));
            session()->flash('message', 'You have Completed a Delivery!');
            return redirect('/completedOrders');
        }
    }

    function deliveryAction(Request $req, $status)
    {

        $driverId = Auth::guard('driver')->user()->drvid;
        $deliveryId = $req->deliveryId;
        $invId = $req->invId;

        $alreadyAssigned = DB::table('tbl_assigned_orders')
            ->where('oid', $invId)
            ->get();

        if ($status == 'cancelled') {

            DB::table('tbl_delivery')->where('id', $deliveryId)->update(array('driver_id' => 0, 'is_scheduled' => 0, 'status' => $status));

            DB::table('tbl_invoices')->where('invoice_id', $invId)->update(array('deliver_by' => 0, 'schedule_for' => 0, 'is_scheduled' => 0, 'order_status' => $status,  'delivery_status' => 1,  'updated_at' => date('Y-m-d H:i:s')));

            DB::table('tbl_delivery_cancelled')->insert(array('delivery_id' => $deliveryId, 'driver_id' => $driverId, 'updated_at' => date('Y-m-d H:i:s')));

            if ($alreadyAssigned->count() != 0) {
                DB::table('tbl_assigned_orders')->where('oid', $invId)->update(array('drvid' => 0, 'status' => $status, 'delivery_date' => date('Y-m-d H:i:s')));
            }


            session()->flash('warningmsg', 'You have cancled a Delivery Order');

            return  redirect('/dashboard');
        } elseif ($status == 'accepted') {

            DB::table('tbl_delivery')->where('id', $deliveryId)->update(array('driver_id' => $driverId, 'status' => $status));

            DB::table('tbl_invoices')->where('invoice_id', $invId)->update(array('order_status' => $status, 'updated_at' => date('Y-m-d H:i:s')));


            if ($alreadyAssigned->count() == 0) {
                DB::table('tbl_assigned_orders')->insert(array('drvid' => $driverId, 'oid' => $invId, 'drop_location' => $req->drop_location_address, 'status' => $status, 'delivery_date' => date('Y-m-d H:i:s')));
            } else {
                DB::table('tbl_assigned_orders')->where('oid', $invId)->update(array('drvid' => $driverId, 'status' => $status, 'drop_location' => $req->drop_location_address, 'delivery_date' => date('Y-m-d H:i:s')));
            }


            session()->flash('message', 'You have accepted a Delivery Order');

            return  redirect('/acceptedOrders');
        } elseif ($status == 'ongoing') {

            $ongoingDelivery = DB::table('tbl_delivery')
                ->where('driver_id', $driverId)
                ->where('status', 'ongoing')
                ->get();

            if ($ongoingDelivery->count() == 0) {

                DB::table('tbl_delivery')->where('id', $deliveryId)->update(array('driver_id' => $driverId, 'status' => $status));

                DB::table('tbl_invoices')->where('invoice_id', $invId)->update(array('order_status' => $status,  'updated_at' => date('Y-m-d H:i:s')));


                if ($alreadyAssigned->count() == 0) {
                    DB::table('tbl_assigned_orders')->insert(array('drvid' => $driverId, 'oid' => $invId, 'drop_location' => $req->drop_location_add, 'status' => $status, 'delivery_date' => date('Y-m-d H:i:s')));
                } else {
                    DB::table('tbl_assigned_orders')->where('oid', $invId)->update(array('status' => $status, 'delivery_date' => date('Y-m-d H:i:s')));
                }
                session()->flash('message', 'You Have a Ongoing Delivery! Check Details Here!');

                return  redirect('/ongoingOrder');
            } else {

                session()->flash('warningmsg', 'Please Deliver your Ongoing Order First');
                return  redirect('/pickupOrders');
            }
        } else {

            session()->flash('warningmsg', 'Please Try Again!');
            return  redirect('/deliveryPool');
        }
    }

    function pickOrder(Request $req, $id)
    {

        $driverId = Auth::guard('driver')->user()->drvid;

        $orders = DB::table('tbl_delivery')
            ->select('tbl_delivery.*', 'tbl_invoices.name', 'tbl_invoices.last_name', 'tbl_invoices.name', 'tbl_invoices.ordernumber', 'tbl_invoices.amount', 'tbl_invoices.payment_method', 'arm_members.Phone')
            ->join('tbl_invoices', 'tbl_delivery.inv_id', '=', 'tbl_invoices.invoice_id')
            ->join('arm_members', 'tbl_invoices.MemberId', '=', 'arm_members.MemberId')
            ->where('tbl_delivery.driver_id', $driverId)
            ->where('tbl_delivery.id', $id)
            ->get();

        $orderCount = $orders->count();
        return view('Driver/pickupdelivery', ['orders' => $orders, 'orderCount' => $orderCount]);
    }

    public function search($value)
    {
        $project = Delivery::query();
        // if (request('term')) {
        $project->where('pick_up', 'Like', '%' . $value . '%');
        // }
        $project->orderBy('id', 'DESC')->paginate(10);
        dd($project);

        return;
    }
}
