<!DOCTYPE html>
<?php
//ini_set('max_execution_time', 300);
?>


<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{asset('img/icon.png')}}">
    <link rel="stylesheet" type="text/css" href="https://pwa.omegaitsys.com/assets/skin/default_skin/css/theme.css">
    <link rel="stylesheet" type="text/css" href="https://pwa.omegaitsys.com/assets/allcp/forms/css/forms.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style type="text/css">
    .field-icon {
        float: right;
        margin-right: 8px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
        cursor: pointer;
        color: #67d3e0;
    }

    .state-error {
        display: block !important;
        margin-top: 6px;
        padding: 0 3px;
        font-family: Arial, Helvetica, sans-serif;
        font-style: normal;
        line-height: normal;
        font-size: 0.85em;
        color: #DE888A;
    }

    .lockscreen-logo {
        font-size: 35px;
        text-align: center;
        margin: 20px 32%;
        font-weight: 300;
        margin-left: 30%;
        height: 150px;
        width: 150px;
        background-color: white;
        border-radius: 50%;
    }

    .alert-danger {
        background-color: #ffe0de;
    }

    .allcp-form {
        margin-top: 0 !important;
    }
</style>
<!-- Login Header -->


<body class="utility-page sb-l-c sb-r-c">

    <!--  Body Wrap   -->
    <div id="main" class="animated fadeIn">

        <!--  Main Wrapper  -->
        <section id="content_wrapper">

            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>

            <!--  Content  -->
            <section id="content">



                <!--  Login Form  -->
                <div class="allcp-form theme-primary mw350" id="login">

                    <div class="lockscreen-logo">
                        <a href="https://pwa.omegaitsys.com/"><img src="https://pwa.omegaitsys.com/uploads/admin/site/logo.png" style="margin:45px -33px;"></a>
                    </div>
                    <div class="panel mw350">

                        <div class="section row mbn">
                            @if ($flash = session('credentialserror'))

                            <div class="alert alert-danger" style="">
                                <span class="close" data-dismiss="alert">×</span>
                                <strong style="text-align:center; color: red;">{{Session::get('credentialserror')}} </strong>
                            </div>
                            @endif
                            @if( Session::get('incorrect') )
                            <div class="alert alert-danger" style="">
                                <span class="close" data-dismiss="alert">×</span>
                                <strong style="text-align:center; color: red;">{{Session::get('incorrect')}} </strong>
                            </div>
                            @endif

                        </div>

                        <form method="post" action="{{url('/driver/auth/login')}}" id="allcp-form">
                            @csrf
                            <h3 style="text-align: center; padding-bottom: 30px;color: #464646;font-weight: 600;">Driver Login</h3>

                            <div class="panel-body pn mv10">

                                <div class="section">
                                    <input type="text" name="username" id="username" class="gui-input" placeholder="Username" value="">

                                </div>
                                <!--  /section  -->

                                <div class="section">
                                    <input type="password" name="password" id="password" class="gui-input" placeholder="Password" value="">

                                    <label for="password" class="field-icon">
                                        <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password" style="color:#67d3e0;cursor:pointer;"></span>

                                    </label>



                                </div>

                                <div class="section" align="center">
                                    <input type="submit" class="btn btn-bordered btn-primary" value="Log in">
                                    <div style="text-align: center; margin: 10px; ">
                                        Doesn't have an accout? <a href="{{url('/register')}}" style="font-weight: 700;color: #0992d8;font-size: 14 ">Register</a> now!

                                    </div>
                                </div>

                            </div>
                            <!--  /section  -->
                            <!--  /Form  -->
                        </form>
                    </div>
                    <!--  /Panel  -->
                </div>
                <!--  /Spec Form  -->

            </section>
            <!--  /Content  -->

        </section>
        <!--  /Main Wrapper  -->

    </div>
    <!--  /Body Wrap   -->

    <!--  Login Footer   -->
    @include('Driver/login_footer')
    <?php //$this->load->view('admin/activemenu');
    ?>
    <script src="https://pwa.omegaitsys.com/assets/allcp/forms/js/jquery.validate.min.js"></script>
    <script src="https://pwa.omegaitsys.com/assets/allcp/forms/js/additional-methods.min.js"></script>
    <?php /*
	
	*/ ?>
    <script type="text/javascript">
        (function($) {

            $(document).ready(function() {

                "use strict";
                // Init Theme Core
                Core.init();

                // Init Demo JS
                Demo.init();

                $.validator.methods.smartCaptcha = function(value, element, param) {
                    return value == param;
                };

                $("#allcp-form").validate({

                    // States

                    errorClass: "state-error",
                    validClass: "state-success",
                    errorElement: "em",

                    // Rules

                    rules: {
                        username: {
                            required: true
                        },
                        password: {
                            required: true,
                            minlength: 6,
                            maxlength: 16
                        }
                    },

                    // error message
                    messages: {
                        username: {
                            required: 'Please enter username'
                        },
                        password: {
                            required: 'Please enter password'
                        }
                    },

                    /* @validation highlighting + error placement
                     ---------------------------------------------------- */

                    highlight: function(element, errorClass, validClass) {
                        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                    },
                    errorPlacement: function(error, element) {
                        if (element.is(":radio") || element.is(":checkbox")) {
                            element.closest('.option-group').after(error);
                        } else {
                            error.insertAfter(element.parent());
                        }
                    }
                });


                $(".toggle-password").click(function() {
                    $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
                    if (input.attr("type") == "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }
                });

            });

        })(jQuery);
    </script>
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
</body>

</html>