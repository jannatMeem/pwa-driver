@extends('layouts.master')

@push('css')
<style>
    .card-header {
        color: black;
    }

    .modal-header {
        background-color: #ffffff00;
        justify-content: center;
        /* background-color: rgba(242, 43, 43, 0.6); */
    }

    .modal-input-group {
        margin-bottom: 5%;
        display: flex;
        flex-wrap: wrap;
    }
</style>
@endpush

@section('content')


<div class="delivery-details">
    @include('includes.flashmsg')
    <div class="delivery-details-text">
        <h5 style="color: #6c6c6c;">Available Delivery Orders</h5>
    </div>
    @if($orderCount == 0)
    <div class="container-fluid">
        <div class="row rounded align-items-center justify-content-center mx-0" style="background-color:#ffffff">
            <div class="col-md-6 text-center">
                <img src="{{asset('img/no data found.jpg')}}" style="width: 100%; height: auto;">
                <h6 class="mb-4" style="font-weight: 500;color: #999999;">No orders available at the moment!</h6>
            </div>
        </div>
    </div>
    @else
    <div class="card-group">
        @foreach($orders as $order)

        <div class="card">
            <div class="card-header">
                <!-- {{ strtoupper ($order->status)}} -->
                Accept this order now!

            </div>
            <div class="card-body">
                <h5 class="card-title">Order# {{ $order->ordernumber}}</h5>
                <!-- <p class="card-text"><i class="fa fa-user fa-fw me-3 text-primary"></i>{{$order->name}}{{' '}}{{$order->last_name}}</p> -->
                <p class="card-text"><i class="fa fa-calendar fa-fw me-3 "></i>{{ date('F j, Y h:i:s a', strtotime($order->expected_delivery_date) ); }}</p>
                <p class="card-text"><i class="fa fa-map-marker fa-fw me-3 "></i>{{$order->pick_up}}</p>
                <p class="card-text"><i class="fa fa-location-arrow fa-fw me-3"></i>{{$order->drop_off}}</p>
                <!-- <p class="card-text"><i class="fa fa-phone fa-fw me-3"></i>{{ $order->Phone }}</p> -->
                <div class="card-button">
                    <a href="#" class="btn btn-primary card-submit-btn" type="submit" onclick="acceptedDelivery( {{$order->id}},{{$order->inv_id}} )">See Details</a>
                </div>
            </div>

        </div>

        @endforeach
    </div>
    @endif
</div>





<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="acceptedDelivery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="betmdLabel">Accepte This Delivery</h5>
                <!-- <button id='closeModal' type="submit" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->

            </div>
            <form action="{{url('/driver/acceptDelivery')}}" method="post" role="form">
                <div class="modal-body">
                    @csrf

                    <div class="modal-input-group">
                        <label>Delivery Date: </label>
                        <input id="date" value="" readonly style="border: none;">
                    </div>

                    <div class="modal-input-group">
                        <label>Delivery Charge:</label>
                        <input id="charge" value="" readonly style="border: none; ">
                    </div>

                    <div class="modal-input-group">
                        <label>Pickup Address: </label>
                        <input id="pick_location_add" value="" readonly style="border: none; width: 100%;">
                    </div>
                    <div class="modal-input-group">
                        <label>Drop-off Address: </label>
                        <input id="drop_location_add" value="" readonly style="border: none;width: 100%;">
                    </div>

                    <div>
                        <label>Location:</label>
                        <small>Pickup: P & Drop-off: D</small>


                        <div class="bg-light rounded">
                            <div class="position-relative rounded" id="pick_location_map" style="height: 500px"></div>
                        </div>

                    </div>

                    <!-- Hidden Inputs -->
                    <input type="hidden" id="deliveryId" name="deliveryId" value="">
                    <input type="hidden" id="invId" name="invId" value="">
                    <input type="hidden" id="pick_latitude" name="pick_latitude" value="" required>
                    <input type="hidden" id="pick_longitude" name="pick_longitude" value="" required>
                    <input type="hidden" id="drop_latitude" name="drop_latitude" value="" required>
                    <input type="hidden" id="drop_longitude" name="drop_longitude" value="" required>
                    <!-- Hidden Inputs -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

                    <button type="submit" class="btn btn-primary">Accept Delivery</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->

@endsection


@push('js')
<script type="text/javascript">
    const labels = "12";
    let labelIndex = 0;
    $(document).ready(function() {

    });

    function acceptedDelivery(deliveryId, invId) {
        $.ajax({
            type: 'GET',
            url: "{{url('/getDelivery')}}/" + deliveryId,
            dataType: 'json',
            encode: true,

            complete: function(data) {
                var response = data.responseJSON;
                var deliveryDate = new Date(response.expected_delivery_date).toLocaleString('en-us', {
                    weekday: "long",
                    year: "numeric",
                    month: "short",
                    day: "numeric"
                });
                $('#date').val(deliveryDate);

                $('#charge').val(response.charge);

                $('#pick_location_add').val(response.pick_up);
                $('#drop_location_add').val(response.drop_off);

                $('#pick_latitude').val(response.pickup_latitude);
                $('#pick_longitude').val(response.pickup_longitude);

                $('#drop_latitude').val(response.drop_latitude);
                $('#drop_longitude').val(response.drop_longitude);

                initialize_pick_map();
                // initialize_drop_map();
            }


        })
        $('#deliveryId').val(deliveryId);
        $('#invId').val(invId);
        $('#acceptedDelivery').modal('show');
    }

    function initialize_pick_map() {

        var lat_value = document.getElementById('pick_latitude').value;
        var long_value = document.getElementById('pick_longitude').value;
        var dlat_value = document.getElementById('drop_latitude').value;
        var dlong_value = document.getElementById('drop_longitude').value;

        var platlng = new google.maps.LatLng(lat_value, long_value);


        var myOptions = {
            zoom: 15,
            center: platlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }



        var map = new google.maps.Map(document.getElementById("pick_location_map"), myOptions);

        var locationArray = [
            ['Pick-Up', lat_value, long_value, 1],
            ['Drop-Off', dlat_value, dlong_value, 2],
        ];

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < locationArray.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locationArray[i][1], locationArray[i][2]),
                map: map,
                label: labels[labelIndex++ % labels.length],
                // icon: 'img/icon/driver-location.png'
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locationArray[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }

</script>

@endpush