@extends('layouts.master')

@push('css')
<style>
    .text-primary {
        color: #ff484cc4 !important;
    }

    .a {
        color: #ff484cc4 !important;
    }

    .statistic-bar {
        background-color: rgba(228, 192, 65, 0.88);
        color: white;
        font-size: larger;
        font-weight: 600;
        display: flex;
        justify-content: space-between;
        padding: 15px 0px;
    }

    .statistic-bar div {
        margin: auto 20px;
    }

    .badge {
        background: white;

    }

    .card-header {
        color: black;
    }

    .modal-header {
        background-color: #ffffff00;
        justify-content: center;
    }

    .modal-input-group {
        margin-bottom: 5%;
        display: flex;
        flex-wrap: wrap;
    }

    @media (max-width: 991.98px) {


        .statistic-div {
            display: none;
        }

        .statistic-div-mobile {
            display: grid !important;
            grid-template-columns: 1fr;
            row-gap: 20px;
        }

        .calender {
            width: 99%;
        }

    }
</style>
@endpush

@section('content')
<!-- Main Container Start -->

<div class="container-table">
    @include('includes.flashmsg')
    <!-- Sale & Revenue Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="statistic-div">
            <div>
                <a href="{{url('/deliveryPool')}}">
                    <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                        <i class="fa fa-chart-line fa-3x text-primary"></i>
                        <div class="ms-3">
                            <p class="mb-2 text-primary">Aabailable Orders</p>
                            <h6 class="mb-0">{{$data['total_available_orders']}}</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div c>
                <a href="{{url('/acceptedOrders')}}">
                    <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                        <i class="fa fa-chart-bar fa-3x text-primary"></i>
                        <div class="ms-3">
                            <p class="mb-2 text-primary">Accepted Orders</p>
                            <h6 class="mb-0">{{$data['total_accepted_orders']}}</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div>
                <a href="{{url('/completedOrders')}}">
                    <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                        <i class="fa fa-chart-area fa-3x text-primary"></i>
                        <div class="ms-3">
                            <p class="mb-2 text-primary">Completed Orders</p>
                            <h6 class="mb-0">{{$data['total_delivered_orders']}}</h6>
                        </div>
                    </div>
                </a>
            </div>
            <div>
                <a href="{{url('/cancelledOrders')}}">
                    <div class="bg-light rounded d-flex align-items-center justify-content-between p-4">
                        <i class="fa fa-chart-pie fa-3x text-primary"></i>
                        <div class="ms-3">
                            <p class="mb-2 text-primary">Cancelled Orders</p>
                            <h6 class="mb-0">{{$data['total_cancelled_orders']}}</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="statistic-div-mobile" style="display:none">

            <a href="{{url('/deliveryPool')}}">
                <div class="statistic-bar">
                    <div> Aabailable Orders </div>
                    <div class="badge rounded">
                        <h6 class="mb-0">{{$data['total_available_orders']}}</h6>
                    </div>
                </div>
            </a>


            <a href="{{url('/acceptedOrders')}}">
                <div class="statistic-bar" style="background-color: #58A2E8;">
                    <div>Accepted Orders</div>
                    <div class="badge rounded">
                        <h6 class="mb-0">{{$data['total_accepted_orders']}}</h6>
                    </div>
                </div>
            </a>

            <a href="{{url('/completedOrders')}}">
                <div class="statistic-bar" style="background-color: #592dff75">
                    <div>Completed Orders</div>
                    <div class="badge rounded">
                        <h6 class="mb-0">{{$data['total_delivered_orders']}}</h6>
                    </div>
                </div>
            </a>

            <a href="{{url('/cancelledOrders')}}">
                <div class="statistic-bar" style="background-color: rgba(242, 43, 43, 0.6);">
                    <div>Cancelled Orders</div>
                    <div class="badge rounded">
                        <h6 class="mb-0">{{$data['total_cancelled_orders']}}</h6>
                    </div>
                </div>
            </a>

        </div>
    </div>
    <!-- Sale & Revenue End -->

    <!-- Recent Sales Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-light text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Delivery Pool</h6>
                <a href="{{url('/deliveryPool')}}" class="text-primary">Show All</a>
            </div>
            <div class="table-responsive">
                @if($data['total_available_orders'] == 0)
                No orders available at the moment!
                @else
                <table class="table text-start align-middle table-bordered table-hover mb-0">
                    <thead>
                        <tr class="text-dark" style="background: #fc707499;">

                            <th scope="col">Order#</th>
                            <th scope="col">Date</th>
                            <th scope="col">Pick Up</th>
                            <th scope="col">Drop Off</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orders as $order)
                        <tr>

                            <td>{{$order->ordernumber}}</td>
                            <td>{{ date('F j, Y h:i:s a', strtotime($order->expected_delivery_date) ); }}</td>
                            <td>{{$order->pick_up}}</td>
                            <td>{{$order->drop_off}}</td>
                            <td>{{strtoupper ($order->status)}}</td>
                            <td><a class="btn btn-sm btn-primary" onclick="acceptedDelivery( {{$order->id}},{{$order->inv_id}} )">Detail</a></td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>

                @endif
            </div>
        </div>
    </div>
    <!-- Recent Sales End -->

</div>




<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="acceptedDelivery" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="betmdLabel">Accepte This Delivery</h5>
                <!-- <button id='closeModal' type="submit" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->

            </div>
            <form action="{{url('/driver/acceptDelivery')}}" method="post" role="form">
                <div class="modal-body">
                    @csrf

                    <div class="modal-input-group">
                        <label>Delivery Date: </label>
                        <input id="date" value="" readonly style="border: none;">
                    </div>

                    <div class="modal-input-group">
                        <label>Delivery Charge:</label>
                        <input id="charge" value="" readonly style="border: none; ">
                    </div>

                    <div class="modal-input-group">
                        <label>Pickup Address: </label>
                        <input id="pick_location_add" value="" readonly style="border: none; width: 100%;">
                    </div>
                    <div class="modal-input-group">
                        <label>Drop-off Address: </label>
                        <input id="drop_location_add" value="" readonly style="border: none;width: 100%;">
                    </div>

                    <div>
                        <label>Location:</label>
                        <small>Pickup: P & Drop-off: D</small>


                        <div class="bg-light rounded">
                            <div class="position-relative rounded" id="pick_location_map" style="height: 500px"></div>
                        </div>

                    </div>

                    <!-- Hidden Inputs -->
                    <input type="hidden" id="deliveryId" name="deliveryId" value="">
                    <input type="hidden" id="invId" name="invId" value="">
                    <input type="hidden" id="pick_latitude" name="pick_latitude" value="" required>
                    <input type="hidden" id="pick_longitude" name="pick_longitude" value="" required>
                    <input type="hidden" id="drop_latitude" name="drop_latitude" value="" required>
                    <input type="hidden" id="drop_longitude" name="drop_longitude" value="" required>
                    <!-- Hidden Inputs -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

                    <button type="submit" class="btn btn-primary">Accept Delivery</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->


@endsection


@push('js')
<script type="text/javascript">
    const labels = "12";
    let labelIndex = 0;

    function acceptedDelivery(deliveryId, invId) {
        $.ajax({
            type: 'GET',
            url: "{{url('/getDelivery')}}/" + deliveryId,
            dataType: 'json',
            encode: true,

            complete: function(data) {
                var response = data.responseJSON;
                var deliveryDate = new Date(response.expected_delivery_date).toLocaleString('en-us', {
                    weekday: "long",
                    year: "numeric",
                    month: "short",
                    day: "numeric"
                });
                $('#date').val(deliveryDate);

                $('#charge').val(response.charge);

                $('#pick_location_add').val(response.pick_up);
                $('#drop_location_add').val(response.drop_off);

                $('#pick_latitude').val(response.pickup_latitude);
                $('#pick_longitude').val(response.pickup_longitude);

                $('#drop_latitude').val(response.drop_latitude);
                $('#drop_longitude').val(response.drop_longitude);

                initialize_pick_map();

            }


        })
        $('#deliveryId').val(deliveryId);
        $('#invId').val(invId);
        $('#acceptedDelivery').modal('show');
    }

    function initialize_pick_map() {

        var lat_value = document.getElementById('pick_latitude').value;
        var long_value = document.getElementById('pick_longitude').value;
        var dlat_value = document.getElementById('drop_latitude').value;
        var dlong_value = document.getElementById('drop_longitude').value;

        var platlng = new google.maps.LatLng(lat_value, long_value);


        var myOptions = {
            zoom: 15,
            center: platlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }



        var map = new google.maps.Map(document.getElementById("pick_location_map"), myOptions);

        var locationArray = [
            ['Pick-Up', lat_value, long_value, 1],
            ['Drop-Off', dlat_value, dlong_value, 2],
        ];

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < locationArray.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locationArray[i][1], locationArray[i][2]),
                map: map,
                label: labels[labelIndex++ % labels.length],
                // icon: 'img/icon/driver-location.png'
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locationArray[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>

@endpush