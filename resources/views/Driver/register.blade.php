<!DOCTYPE html>


<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="{{asset('img/icon.png')}}">
  <link rel="stylesheet" type="text/css" href="https://pwa.omegaitsys.com/assets/skin/default_skin/css/theme.css">
  <link rel="stylesheet" type="text/css" href="https://pwa.omegaitsys.com/assets/allcp/forms/css/forms.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<style type="text/css">
  body {
    height: auto;
    background: #ffffff;
  }

  .field-icon {
    float: right;
    margin-right: 8px;
    margin-top: -25px;
    position: relative;
    z-index: 2;
    cursor: pointer;
    color: #67d3e0;
  }

  .state-error {
    display: block !important;
    margin-top: 6px;
    padding: 0 3px;
    font-family: Arial, Helvetica, sans-serif;
    font-style: normal;
    line-height: normal;
    font-size: 0.85em;
    color: #DE888A;
  }

  .alert-danger {
    background-color: #ffe0de;
  }

  .img-responsive {
    width: 10%;
  }

  .allcp-form {
    margin-top: none;
  }

  .row {
    background: #94cdff;
    margin: 5%;
    border-radius: 5px;
  }

  .single_feature {
    margin: 3% 3%;
  }

  .form-header-text {
    text-align: center;
    padding: 3% 5%;
    color: #4e4e4e;
  }

  label {
    color: #505050;
  }

  .loginbtn {
    width: 98%;
    color: #ffffff;
    background-color: #3180eded;
    margin: 5% 1%;
  }
</style>
<!-- Login Header -->


<body class="utility-page sb-l-c sb-r-c">

  <!--  Body Wrap   -->
<section>
<nav class="navbar navbar-expand navbar-light sticky-top px-4 py-0" style="  display: flex;">

    <a href="{{asset('')}}" class="navbar-brand d-flex me-4" style="margin: auto;">
      <img class=" me-lg-2" src="{{asset('img/F2N.png')}}" alt="logo" style="width: auto; height: 40px">
    </a>

</nav>
</section>
 
  <section id="features">
    <div class="container">
      <div class="feature_content_area wow fadeInUp">
        <div class="row" style="margin-top: 10px;">
          <div class="col-md-12 col-sm-12">
            <div class="single_feature">
              <form action="{{url('/driver/create')}}" method="post" enctype="multipart/form-data">
                @csrf

                <!-- HIDDEN INPUTS -->
                <input type="hidden" id="latitude" name="latitude" class="form-control" required="">
                <input type="hidden" id="longitude" name="longitude" class="form-control" required="">
                <!-- HIDDEN INPUTS -->

                <h3 class="form-header-text">Driver Registration</h3>
                <div class="form-group">
                  <label for="exampleFormControlInput1">Username</label>
                  <input type="text" class="form-control" name="username" placeholder="Enter username" required="" aria-label="username" value="{{old('username')}}">
                  <span style="color:red">@error('username'){{$message}}@enderror</span>
                </div>


                <div class="form-group">
                  <label for="exampleFormControlInput1">First Name</label>
                  <input type="text" class="form-control" name="fname" placeholder="Enter First name" required="" aria-label="fname" value="{{old('fname')}}">
                  <span style="color:red">@error('fname'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlInput2">Middle Name</label>
                  <input type="text" class="form-control" name="mname" placeholder="Enter username" required="" aria-label="mname" value="{{old('mname')}}">
                  <span style="color:red">@error('mname'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlInput3">Last Name</label>
                  <input type="text" class="form-control" name="lname" placeholder="Enter username" required="" aria-label="lname" value="{{old('lname')}}">
                  <span style="color:red">@error('lname'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="image"><b>Profile image</b> </label>

                  <input class="form-control" type="file" id="img_urlAdd1" name="profileImage" value="{{old('profileImage')}}" aria-label="profileImage">
                  <img id='img-uploadAdd1' class="img-responsive" />
                  <!-- <small id="emailHelp" class="form-text text-muted">require image width:800px height:400px</small> -->
                  <span style="color:red">@error('profileImage'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlInput1">Gender</label>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="gender1" value="male" checked>
                    <label class="form-check-label" for="gender">
                      Male
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="gender" id="gender2" value="female">
                    <label class="form-check-label" for="gender2">
                      Female
                    </label>
                  </div>

                </div>

                <div class="form-group">
                  <label for="exampleFormControlInput1">Phone</label>
                  <input type="text" class="form-control" name="phone" placeholder="Enter Phone" required="" aria-label="phone" value="{{old('phone')}}">
                  <span style="color:red">@error('phone'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Address</label>
                  <textarea type="text" class="form-control" name="address" placeholder="Enter Address" required="" aria-label="address" value="{{old('address')}}"></textarea>
                  <span style="color:red">@error('Address'){{$message}}@enderror</span>
                </div>

                <div class="form-group">

                  <label for="exampleFormControlInput1">City</label>

                  <select name="city" class="form-control" id="city" role="dialog" aria-hidden="true">
                    <option>Select a City</option>
                    @foreach($city as $key)
                    <option value='{{$key->id}}'>{{$key->label}}</option>
                    @endforeach
                  </select>
                  <span style="color:red">@error('city'){{$message}}@enderror</span>
                </div>


                <div class="form-group">
                  <label for="exampleFormControlInput1">Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Enter Password" required="" aria-label="password">
                  <span style="color:red">@error('password'){{$message}}@enderror</span>
                </div>






                <div class="form-group">
                  <label for="exampleFormControlInput1">Types of Vehicles</label>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="vehicles1" class="custom-control-input" name="vehicles[]" value="motorcycle">
                    <label class="custom-control-label" for="vehicles">Motorcycle</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="vehicles2" class="custom-control-input" name="vehicles[]" value="car">
                    <label class="custom-control-label" for="vehicles">Car</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="vehicles3" class="custom-control-input" name="vehicles[]" value="auv">
                    <label class="custom-control-label" for="vehicles">AUV</label>
                  </div>
                  <span style="color:red">@error('vehicles'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlInput1">Modal</label>
                  <input type="number" class="form-control" name="modal" placeholder="Enter Modal" required="" aria-label="modal" value="{{old('modal')}}">
                  <span style="color:red">@error('modal'){{$message}}@enderror</span>
                </div>

                <div class="form-group">
                  <label for="exampleFormControlInput1">Brand</label>
                  <input type="text" class="form-control" name="brand" placeholder="Enter Brand" required="" aria-label="brand" value="{{old('brand')}}">
                  <span style="color:red">@error('brand'){{$message}}@enderror</span>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlInput1">Referred by</label>
                  <input type="text" class="form-control" name="referredby" placeholder="Referred by" required="" aria-label="referredby" value="{{old('referredby')}}">
                  <span style="color:red">@error('referredby'){{$message}}@enderror</span>
                </div>
                <div class="form-group">

                  <label for="image"><b>Driver's License</b> </label>
                  <input class="form-control" type="file" id="img_urlAdd2" name="drvlicense" value="{{old('drvlicense')}}" aria-label="licenseImage">
                  <img id='img-uploadAdd2' class="img-responsive" />
                  <span style="color:red">@error('drvlicense'){{$message}}@enderror</span>
                </div>


                <div class="form-group">

                  <label for="image"><b>Vehicle Reciept</b> </label>
                  <input class="form-control" type="file" id="img_urlAdd3" name="voreceipt" value="{{old('voreceipt')}}" aria-label="licenseImage">
                  <img id='img-uploadAdd3' class="img-responsive" />
                  <span style="color:red">@error('voreceipt'){{$message}}@enderror</span>
                </div>

                <div class="form-group">

                  <label for="image"><b>Vehicle Image</b> </label>
                  <input class="form-control" type="file" id="img_urlAdd4" name="vcregistration" value="{{old('vcregistration')}}" aria-label="licenseImage">
                  <img id='img-uploadAdd4' class="img-responsive" />
                  <span style="color:red">@error('vcregistration'){{$message}}@enderror</span>
                </div>



                <div class="form-group" style="font-size: 13;">
                  <button type="submit" name="login" class="btn btn-info loginbtn">Sign Up</button>
                  <div style="text-align: center;">
                    Already have an accout? <a href="{{url('/login')}}" style="font-weight: 700;color: #0992d8;font-size: 14 ">Login</a>

                  </div>

                </div>

              
              </form>
            </div>
          </div> <!-- END COL -->

        </div> <!-- END ROW -->
      </div>
    </div> <!-- END CONTAINER -->
  </section>


  <!--  /Body Wrap   -->

  <!--  Login Footer   -->
  @include('Driver/login_footer')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA==" crossorigin="anonymous" />
  <script src="https://pwa.omegaitsys.com/assets/allcp/forms/js/jquery.validate.min.js"></script>
  <script src="https://pwa.omegaitsys.com/assets/allcp/forms/js/additional-methods.min.js"></script>


  <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_Gjdm_0nJk17UVBPoV5Im40uQeguoRAo"></script>

  <script>
    function readURL1(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-uploadAdd1').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#img_urlAdd1").change(function() {
      readURL1(this);
    });


    function readURL2(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-uploadAdd2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#img_urlAdd2").change(function() {
      readURL2(this);
    });

    function readURL3(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-uploadAdd3').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#img_urlAdd3").change(function() {
      readURL3(this);
    });


    function readURL4(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-uploadAdd4').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#img_urlAdd4").change(function() {
      readURL4(this);
    });
  </script>

</body>

</html>