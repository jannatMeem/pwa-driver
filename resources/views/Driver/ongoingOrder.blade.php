@extends('layouts.master')

@push('css')
<style>
    .delivery-details {
        padding: 0% 5%;
    }

    .card-group {
        display: grid;
        grid-template-columns: 1fr;
    }

    .card {
        max-width: 100%;
        width: 100%;
        margin-bottom: 11%;
    }

    .card-header {
        color: white;
        background-color: rgba(242, 43, 43, 0.6) !important;
    }



    .card-button {
        text-align: end;
        margin: 15px;
    }

    .card-button .ongoing {
        color: white;
        font-weight: 500;
        width: 100%;
        background-color: #F22B2B;
        border-color: #F22B2B;
    }

    .card-button .pickup {
        color: white;
        font-weight: 500;
        width: 100%;
        background-color: #a13dd5;
        border-color: #a13dd5;
    }

    .card-header {
        display: block;
    }

    /* .modal-header {
        background-color: #fb4848d1;
    } */

    .modal-title {
        color: white;
    }

    .modal-body-text {
        color: #020101;
        font-size: smaller;
        text-align: center;
    }

    .modal-footer {
        flex-direction: column;
        display: flex;
        justify-content: space-between;
        align-content: center;
        border-top: none;
    }

    .modal-footer button {
        width: 45%;
        font-size: 15px;
    }

    .modal-footer #actionbtn {
        width: 80%;
        height: 40px;
        background: rgba(228, 192, 65, 0.88);
        box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
        border-color: rgba(228, 192, 65, 0.88);
        color: black;
        border-radius: 0;
        font-size: 15px;
        font-weight: 700;
    }

    .modal-footer #cancelbtn {
        color: #F22B2B !important;
        font-size: 15px;
        font-weight: 700;
        background-color: none !important;
        border-color: none !important;
    }


    .ongoing-card {
        padding: 0;
        background: none;
    }

    .ongoing-card-responsive {
        display: none;
    }

    .icon-class {
        color: #f78080;
    }

    img.img-responsive {
        height: 0px;
    }

    .card-body-details {
        display: flex;
        flex-direction: column;
        padding: 3% 3%;
    }

    .card-body-details>div {
        margin-bottom: 1rem;
    }

    .location-div {
        background: #fdfdfd;
        width: 90%;
        margin: auto;
        padding: 10px 10px;
        margin-bottom: 10px;
        border-radius: 5px;
        box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
        font-size: smaller;
    }

    form {
        width: 90%;
        margin: auto;
    }

    @media (max-width: 991.98px) {

        .card {
            position: absolute;
            top: 230px;
            left: 8%;
            right: 8%;
            width: auto;
        }

        .card-header {
            display: none;
        }

        .ongoing-card {
            padding: 110px;
            width: auto;
            margin: auto;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            background-color: rgba(242, 43, 43, 0.6) !important;
        }

        .ongoing-card-responsive {
            display: block;
            position: absolute;
            top: 171px;
            color: white;
            left: 10%;
            font-size: 23px;
        }

        .sidebar {
            height: auto;
        }



    }
</style>
@endpush

@section('content')
<!-- Main Container Start -->

<div class="delivery-details">
    @include('includes.flashmsg')
    <div class="delivery-details-text">
        <!-- <h5>Your Ongoing Delivery Order</h5> -->
    </div>


    @if($orderCount == 0)
    <div class="container-fluid">
        <div class="row rounded align-items-center justify-content-center mx-0" style="background-color:#ffffff">
            <div class="col-md-6 text-center">
                <img src="{{asset('img/no data found.jpg')}}" style="width: 100%; height: auto;">
                <h6 class="mb-4" style="font-weight: 500;color: #999999;">No orders available at the moment!</h6>
            </div>
        </div>
    </div>
    @else

    <div class="card-group ongoing-card">
        @foreach($orders as $order)
        <div class="ongoing-card-responsive">
           
            Deliver the order now!
           
        </div>
        <div class="card">
            <div class="card-header">
                Deliver the order now!
            </div>
            <div class="card-body">
                <div style="display: flex; justify-content: space-between;">
                    <div>
                        {{ date('F j, Y h:i:s a', strtotime($order->expected_delivery_date) ); }}
                    </div>

                    <div>
                        # {{ $order->ordernumber}}
                    </div>

                </div>

                <hr class="dropdown-divider">
                <div class="card-body-details">
                    <div class="card-body-details-1">
                        <p class="card-text"><i class="fa fa-user fa-fw me-3 icon-class"></i>{{$order->name}}{{' '}}{{$order->last_name}}</p>
                        <p class="card-text"><i class="fa fa-credit-card fa-fw me-3 icon-class"></i>COD</p>
                        <!-- <p class="card-text"><i class="fa fa-usd fa-fw me-3 icon-class"></i>{{$order->amount}}</p> -->

                    </div>
                    <div class="card-body-details-2">

                        <p class="card-text"><i class="fa fa-map-marker fa-fw me-3 icon-class"></i>Pickup</p>
                        <p style="margin-left: 5%;">{{$order->pick_up}}</p>
                        <p class="card-text"><i class="fa fa-location-arrow fa-fw me-3 icon-class"></i>Destination</p>
                        <p style="margin-left: 5%;">{{$order->drop_off}}</p>

                    </div>
                </div>
  
                <div>
                    <div class="location-div">
                        <label class="card-text" style="font-size: medium;">Delivery Location</label>
                        <br>
                        {{$order->drop_off}}
                    </div>

                    <input type="hidden" id="drop_latitude" name="drop_latitude" value="{{$order->drop_latitude}}" required>
                    <input type="hidden" id="drop_longitude" name="drop_longitude" value="{{$order->drop_longitude}}" required>
                    <div id="drop_location_map" style="height: 500px"></div>
                </div>
               
                <div class="card-phone">
                    <div>
                        <p class="card-text" style="font-size: medium;">Order Contact: {{ $order->Phone }}</p>
                    </div>
                    <div class="card-phone-links">
                        <a href="tel:{{ $order->Phone }}"><i class="fa fa-phone fa-fw me-3"></i></a>
                        <a href="sms:{{ $order->Phone }}"><i class="fa fa-commenting fa-fw me-3"></i></a>
                    </div>
                </div>

            </div>
            <div class="card-button">
                
                <div class="btn btn-sucess card-submit-btn" type="button" onclick="changeStatusDone( {{ $order->id }},{{ $order->inv_id}} )">Droped Delivery!</div>
                
            </div>
        </div>

        <!-- hidden inputs -->
        <input type="hidden" id="deliveryStatus" value="{{$order->status}}">
        <input type="hidden" id="pickupAdd" value="{{$order->pick_up}}">
        <input type="hidden" id="dropoffAdd" value="{{$order->drop_off}}">
        <!-- hidden inputs -->

        @endforeach
    </div>

    @endif
</div>







<!-- Modal -->
<div class="modal" id="deliveryDone" tabindex="-1" role="dialog" aria-labelledby="betmdLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="betmdLabel">Confirm Delivery!</h5>
            </div> -->
            <div class="modal-body">
                <p class="modal-body-text" style="color: black;">Are you sure all costs have been settled with the user?</p>



                <form action="{{url('driver/delivery/status')}}" method="post" role="form" enctype="multipart/form-data">

                    @csrf
                    <div class="location-div" style="background: #f9f9f9;box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);">
                        <label class="card-text" style="font-size: medium;">Drop-off Location</label>
                        <br>

                        <p type="text" id="deliveryModalAddress">
                        <p>
                    </div>

                    <br>

                    <div class="form-group">
                        <p for="recepientName"> <b>Recepient's Name:</b> </p>
                        <input type="text" id="recepientName" class="form-control" name="recepientName" required="" aria-label="recepientName" value="{{old('recepientName')}}">
                    </div>
                    <br>


                    <div class="form-group">
                        <p for="image"> <b>Signature</b> </p>
                        <input class="form-control" type="file" id="img_urlAdd" name="signature" value="{{old('signature')}}" aria-label="signature" required>
                        <img id='img-uploadAdd' class="img-responsive" />

                    </div>
                    <br>


                    <input type="hidden" id="deliveryId1" name="deliveryId1" value="">
                    <input type="hidden" id="invId1" name="invId1" value="">
                    <input type="hidden" id="status" name="status" value="delivered">
                    <div class="modal-footer">
                        <button type="submit" id="actionbtn" class="btn btn-primary">Yes, Amount Settled</button>
                        <button type="button" class="btn" id="cancelbtn" data-bs-dismiss="modal">Return</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->

@endsection


@push('js')

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
<script type="text/javascript">
    const labels = "UD";
    let labelIndex = 0;
    var driverLat;
    var driverLong;
    var Dlatlng;
    $(document).ready(function() {

        var status = $('#deliveryStatus').val();
        getDriverLocation();

    });


    var init = window.setInterval(function() {
        getDriverLocation();
    }, 50000);



    function getDriverLocation() {

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(pickSuccess);
        } else {
            console.log("location not supported");

        }
    }


    function pickSuccess(position) {
        var status = $('#deliveryStatus').val();
        addition = position.coords.latitude + "," + position.coords.longitude;

        driverLat = position.coords.latitude;
        driverLong = position.coords.longitude;
        Dlatlng = new google.maps.LatLng(driverLat, driverLong);
      
            initialize_drop_map();
        


    }

    function initialize_drop_map() {

        var lat_value = document.getElementById('drop_latitude').value;
        var long_value = document.getElementById('drop_longitude').value;

        var latlng = new google.maps.LatLng(lat_value, long_value);


        var myOptions = {
            zoom: 14,
            center: Dlatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById("drop_location_map"), myOptions);


        var locationArray = [
            ['Driver-Location', driverLat, driverLong, 1],
            ['Delivery-Location', lat_value, long_value, 2],
        ];

        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < locationArray.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locationArray[i][1], locationArray[i][2]),
                map: map,
                label: labels[labelIndex++ % labels.length],
                // icon: 'img/icon/driver-location.png'
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locationArray[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-uploadAdd').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#img_urlAdd").change(function() {
        readURL(this);
        $(".img-responsive").css("height", "150px");
    });


    function changeStatusDone(Id, invId) {
        $('#deliveryId1').val(Id);
        $('#invId1').val(invId);
        var pickupAdd = $('#pickupAdd').val();
        var dropoffAdd = $('#dropoffAdd').val();

        $('#deliveryModalAddress').html(dropoffAdd);

        $('#deliveryDone').show();

        $('#deliveryDone').modal('toggle');

    }
</script>

@endpush