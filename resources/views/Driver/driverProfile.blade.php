@extends('layouts.master')

@push('css')
<style>
    .gradient-custom {
        /* fallback for old browsers */
        background: #f6d365;

        /* Chrome 10-25, Safari 5.1-6 */
        background: -webkit-linear-gradient(to right bottom, rgba(246, 211, 101, 1), rgba(253, 160, 133, 1));

        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right bottom, rgba(246, 211, 101, 1), rgba(253, 160, 133, 1))
    }

    .card {
        border-radius: .5rem;
        margin-top: 2%;
    }

    .sidebar {
        height: auto;
    }

    .modal-footer {
        flex-direction: column;
        display: flex;
        justify-content: space-between;
        align-content: center;
        border-top: none;
    }

    .modal-footer button {
        width: 45%;
        font-size: 15px;
    }

    .modal-footer #actionbtn {
        width: 80%;
        height: 40px;
        background: rgba(228, 192, 65, 0.88);
        box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
        border-color: rgba(228, 192, 65, 0.88);
        color: black;
        border-radius: 0;
        font-size: 15px;
        font-weight: 700;
    }

    .modal-footer #cancelbtn {
        color: #F22B2B !important;
        font-size: 15px;
        font-weight: 700;
        background-color: none !important;
        border-color: none !important;
    }
</style>
@endpush

@section('content')
<!-- Main Container Start -->


<div class="container-table">
    <div class="Contact-Section wrapper ispage aboutsec1">

        <!-- FLASH MESSAGE-->
        @include('includes.flashmsg')
        <!-- FLASH MESSAGE END-->
        <section style="background-color: #ffffff">
            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4 gradient-custom text-center text-white" style="border-top-left-radius: .5rem; border-bottom-left-radius: .5rem;  ">

                        <div onclick="profilePicRemove()">
                            <img src="{{asset('')}}{{Auth::guard('driver')->user()->profile_img}}" alt="Driver Profile" style="cursor: pointer;  margin: 10% 0;border-radius: 50% 50% 50% 50%; width: 200px; height: 200px;">
                        </div>




                        <h5>{{Auth::guard('driver')->user()->fname}}{{' '}}
                            {{Auth::guard('driver')->user()->mname}}{{' '}}
                            {{Auth::guard('driver')->user()->lname}}
                        </h5>
                        <p>PWA Driver</p>
                        <a href="{{url('/settings')}}" style="color:white"><i class="far fa-edit mb-5"></i></a>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body p-4">
                            <h6>Personal</h6>
                            <hr class="mt-0 mb-4">
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>First Name</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->fname}}</p>
                                </div>
                                <div class="col-6 mb-3">
                                    <h6>Middle Name</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->mname}}</p>
                                </div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>Last Name</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->lname}}</p>
                                </div>
                                <div class="col-6 mb-3">
                                    <h6>Gender</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->gender}}</p>
                                </div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>Address</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->addr}}</p>
                                </div>
                                <div class="col-6 mb-3">
                                    <h6>City</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->city}}</p>
                                </div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>Phone</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->phone}}</p>
                                </div>

                            </div>
                            <h6>Professional</h6>
                            <hr class="mt-0 mb-4">
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>Model</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->modal}}</p>
                                </div>
                                <div class="col-6 mb-3">
                                    <h6>Brand</h6>
                                    <p class="text-muted">{{Auth::guard('driver')->user()->brand}}</p>
                                </div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>Driving License</h6>
                                    <img src="{{asset('')}}{{Auth::guard('driver')->user()->drvlicense}}" style="width: 100%; height: auto;">
                                </div>
                                <div class="col-6 mb-3">
                                    <h6>Vehicle Reciept</h6>
                                    <img src="{{asset('')}}{{Auth::guard('driver')->user()->voreceipt}}" style="width:  100%; height: auto;">
                                </div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-6 mb-3">
                                    <h6>Vehicle Image</h6>
                                    <img src="{{asset('')}}{{Auth::guard('driver')->user()->vcregistration}}" alt="sample" style="width: 100%; height: auto;">
                                </div>

                            </div>

                            <h6>Social</h6>
                            <hr class="mt-0 mb-4">

                            <a href="#!"><i class="fab fa-facebook-f fa-lg me-3"></i></a>
                            <a href="#!"><i class="fab fa-twitter fa-lg me-3"></i></a>
                            <a href="#!"><i class="fab fa-instagram fa-lg"></i></a>

                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
</div>

<div class="modal" id="pictureremove" tabindex="-1" role="dialog" aria-labelledby="betmdLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="betmdLabel">Confirm Delivery!</h5>
            </div> -->
            <div class="modal-body">
                <p class="modal-body-text" style="color: black;">Do you want to remove profile picture?</p>



                <form action="{{url('profile/picture/remove')}}" method="post" role="form">

                    @csrf

                    <br>


                    <div class="modal-footer">
                        <button type="submit" id="actionbtn" class="btn btn-primary">Yes,remove picture</button>
                        <button type="button" class="btn" id="cancelbtn" data-bs-dismiss="modal">Return</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@push('js')
<script type="text/javascript">
    $(document).ready(function() {

    });

    function profilePicRemove() {
        $('#pictureremove').show();
        $('#pictureremove').modal('toggle');
    }
</script>

@endpush