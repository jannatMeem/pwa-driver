@extends('layouts.master')

@push('css')
<style>
    .driverInfo {
        margin: 20px 8%;
    }

    .formclass {
        width: 70%;
    }

    .form-group {
        margin: 10px;
    }

    .img-responsive {
        width: 10%;
    }
</style>
@endpush

@section('content')
<!-- Main Container Start -->


<div class="container-table">
    <div class="Contact-Section wrapper ispage aboutsec1">

        <!-- FLASH MESSAGE-->
        @include('includes.flashmsg')
        <!-- FLASH MESSAGE END-->

        <div class="driverInfo">
            <div class="formclass">

                <form action="{{url('driver/driverupdate')}}" method="post" enctype="multipart/form-data">

                    <h3 style="text-align: center; padding-bottom: 30px;color: #464646;font-weight: 600;">Update Profile</h3>
                    @csrf

                    <div class="form-group">
                        <input type="hidden" class="form-control" name="id" value="{{Auth::guard('driver')->user()->dvrid}}">
                    </div>
                    <!-- <div class="form-group">
                            <input type="file" class="form-control" name="profileImg">
                        </div> -->

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">First Name</h6>
                        <input type="text" class="form-control" name="firstName" value="{{Auth::guard('driver')->user()->fname}}">
                        <span style="color:red">@error('firstName'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Middle Name</h6>
                        <input type="text" class="form-control" name="middleName" value="{{Auth::guard('driver')->user()->mname}}">
                        <span style="color:red">@error('middleName'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Last Name</h6>
                        <input type="text" class="form-control" name="lastName" value="{{Auth::guard('driver')->user()->lname}}">
                        <span style="color:red">@error('lastName'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Gender</h6>
                        <div style="display:flex">
                            <div class="form-check">
                                @if(Auth::guard('driver')->user()->gender == 'male' )
                                <input class="form-check-input" type="radio" name="gender" id="exampleRadios1" value="male" checked>
                                @else
                                <input class="form-check-input" type="radio" name="gender" id="exampleRadios1" value="male">
                                @endif
                                <label class="form-check-label" for="exampleRadios1">
                                    Male
                                </label>
                            </div>
                            <div class="form-check">
                                @if(Auth::guard('driver')->user()->gender == 'female' )
                                <input class="form-check-input" type="radio" name="gender" id="exampleRadios2" value="female" checked>
                                @else
                                <input class="form-check-input" type="radio" name="gender" id="exampleRadios2" value="female">
                                @endif
                               
                                <label class="form-check-label" for="exampleRadios2">
                                    Female
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Address</h6>
                        <textarea type="text" class="form-control" name="address" value="">{{Auth::guard('driver')->user()->addr}}</textarea>
                        <span style="color:red">@error('Address'){{$message}}@enderror</span>
                    </div>

                    <!-- <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">City Name</h6>
                        <input type="text" class="form-control" name="city" value="{{Auth::guard('driver')->user()->city}}">
                    </div> -->

                    <div class="form-group">

                        <label for="exampleFormControlInput1">City</label>

                        <select name="city" id="city" role="dialog" aria-hidden="true">

                            @foreach($city as $key)

                            @if(Auth::guard('driver')->user()->cid == $key->id )
                            <option value='{{$key->id}}' selected>{{$key->label}}</option>
                            @else
                            <option value='{{$key->id}}'>{{$key->label}}</option>
                            @endif
                            @endforeach
                        </select>
                        <span style="color:red">@error('city'){{$message}}@enderror</span>
                    </div>


                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Phone</h6>
                        <input type="number" class="form-control" name="phone" value="{{Auth::guard('driver')->user()->phone}}">
                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Brand</h6>
                        <input type="text" class="form-control" name="brand" value="{{Auth::guard('driver')->user()->brand}}">
                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Model</h6>
                        <input type="text" class="form-control" name="modal" value="{{Auth::guard('driver')->user()->modal}}">
                        <span style="color:red">@error('modal'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <h6 style="color: #939393;font-size: 10px;">Password</h6>
                        <input type="text" class="form-control" name="password">
                        <span style="color:red">@error('password'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <label for="image"><b>Profile image</b> </label>
                        <input class="form-control" type="file" id="img_urlAdd1" name="profileImage" value="{{old('profileImage')}}" aria-label="profileImage">
                        <img id='img-uploadAdd1' class="img-responsive" />
                        <span style="color:red">@error('profileImage'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <label for="image"><b>Driver's License</b> </label>
                        <input class="form-control" type="file" id="img_urlAdd2" name="drvlicense" value="{{old('drvlicense')}}" aria-label="licenseImage">
                        <img id='img-uploadAdd2' class="img-responsive" />
                        <span style="color:red">@error('drvlicense'){{$message}}@enderror</span>
                    </div>


                    <div class="form-group">
                        <label for="image"><b>Vehicle Reciept</b> </label>
                        <input class="form-control" type="file" id="img_urlAdd3" name="voreceipt" value="{{old('voreceipt')}}" aria-label="licenseImage">
                        <img id='img-uploadAdd3' class="img-responsive" />
                        <span style="color:red">@error('voreceipt'){{$message}}@enderror</span>
                    </div>

                    <div class="form-group">
                        <label for="image"><b>Vehicle Image</b> </label>
                        <input class="form-control" type="file" id="img_urlAdd4" name="vcregistration" value="{{old('vcregistration')}}" aria-label="licenseImage">
                        <img id='img-uploadAdd4' class="img-responsive" />
                        <span style="color:red">@error('vcregistration'){{$message}}@enderror</span>
                    </div>



                    <div class="form-group" style="font-size: 12;">
                        <button type="submit" name="updateBtn" id="loginbtn" class="btn btn-success">Update</button>
                    </div>

                </form>

            </div>
        </div>

    </div>
</div>

@endsection


@push('js')
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
<script>
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-uploadAdd1').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#img_urlAdd1").change(function() {
        readURL1(this);
    });


    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-uploadAdd2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img_urlAdd2").change(function() {
        readURL2(this);
    });

    function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-uploadAdd3').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img_urlAdd3").change(function() {
        readURL3(this);
    });


    function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img-uploadAdd4').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img_urlAdd4").change(function() {
        readURL4(this);
    });
</script>
@endpush