@extends('layouts.master')

@push('css')
<style>
    @media (max-width: 991.98px) {
        .card-body-details {
            flex-direction: column;
        }

    }
    .card-header{
        display: flex;
    justify-content: space-between;
    }
    .icon-class {
        color: #f78080;
    }
</style>
@endpush

@section('content')
<div class="delivery-details">
    @include('includes.flashmsg')
    <div class="delivery-details-text">
        <h5 style="color: #6c6c6c;">Completed Delivery Orders</h5>
    </div>

    @if($orderCount == 0)
    <div class="container-fluid">
        <div class="row rounded align-items-center justify-content-center mx-0" style="background-color:#ffffff">
            <div class="col-md-6 text-center">
                <img src="{{asset('img/no data found.jpg')}}" style="width: 100%; height: auto;">
                <h6 class="mb-4" style="font-weight: 500;color: #999999;">No orders available at the moment!</h6>
            </div>
        </div>
    </div>
    @else

    <div class="card-group">
        @foreach($orders as $order)

        <div class="card">
            <div class="card-header">
                <div>
                    You have completed this order!
                </div>

                <div><i class="fa fa-check-square-o" aria-hidden="true" style="color: green;text-align: end;"></i>
                </div>
            </div>
            <div class="card-body">
                <h5 class="card-title">Order# {{ $order->ordernumber}}</h5>
                <p class="card-text"><i class="fa fa-user fa-fw me-3 "></i>{{$order->name}}{{' '}}{{$order->last_name}}</p>
                <p class="card-text"><i class="fa fa-calendar fa-fw me-3"></i>{{ date('F j, Y h:i:s a', strtotime($order->expected_delivery_date) ); }}</p>
                <div class="card-button">
                    <a href="#" class="btn btn-primary  card-submit-btn" type="button" onclick="deliveryAction( {{$order->id}})">See Details</a>
                </div>
            </div>

        </div>

        @endforeach
    </div>
    @endif

</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="deliveryView" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- <div class="modal-header">

                <h5 class="modal-title" id="betmdLabel">Order Details</h5>
                <button id='closeModal' type="button" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body">
                <p class="modal-body-text"></p>



                <div class="card-body">

                    <div class="card-body-details">
                        <div class="card-body-details-1">

                            <p class="card-text"><b>Status: </b>
                            <h6 id="m-status" value="" style="border: none;"></h6>
                            </p>

                            <p class="card-text"><b>Date: </b>
                            <h6 id="m-date" style="border: none;"></h6>
                            </p>

                            <p class="card-text"><b>Recepient: </b>
                            <h6 id="m-recepient" style="border: none;"></h6>
                            </p>

                        </div>
                        <div class="card-body-details-2">


                            <p class="card-text"><b>Pickup: </b>
                            <h6 id="m-pickadd" style="border: none;"></h6>
                            </p>

                            <p class="card-text"><b>Drop-off: </b>
                            <h6 id="m-dropadd" style="border: none;"></h6>
                            </p>

                            <p class="card-text"><b>Charge: </b>
                            <h6 id="m-charge" style="border: none;"></h6>
                            </p>


                        </div>
                        <div>
                            <p class="card-text"><b>Signeture: </b></p>
                            <img src="" alt="Signeture" id="m-signeture" style=" width: 200px; height: 200px;">
                        </div>

                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->



@endsection


@push('js')
<script type="text/javascript">
    $(document).ready(function() {

    });

    function deliveryAction(deliveryId) {
        $.ajax({
            type: 'GET',
            url: "{{url('/getDelivery')}}/" + deliveryId,
            dataType: 'json',
            encode: true,

            complete: function(data) {
                var response = data.responseJSON;
                // console.log(response.signature);
                var url = "{{ asset('') }}";
                $('#m-date').html(response.delivery_date);
                $('#m-charge').html(response.charge);
                $('#m-signature').html(response.signature);

                $('#m-status').html(response.status);
                $('#m-recepient').html(response.recepientName);

                $('#m-dropadd').html(response.drop_off);
                $('#m-pickadd').html(response.pick_up);

                $("#m-signeture").attr("src", url + response.signature);

            }


        })
        $('#deliveryId').val(deliveryId);
        $('#deliveryView').modal('show');
    }
</script>

@endpush