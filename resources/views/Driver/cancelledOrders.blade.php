@extends('layouts.master')

@push('css')
<style>
    /* .card-header {
        background-color: rgb(241 45 45 / 86%);
        font-size: larger;
        color: white;
        font-weight: 500;
    } */
</style>
@endpush

@section('content')
<div class="delivery-details">
    @include('includes.flashmsg')
    <div class="delivery-details-text">
        <h5 style="color: #6c6c6c;">Cancelled Delivery Orders</h5>
    </div>

    @if($orderCount == 0)
    <div class="container-fluid">
        <div class="row rounded align-items-center justify-content-center mx-0" style="background-color:#ffffff">
            <div class="col-md-6 text-center">
                <img src="{{asset('img/no data found.jpg')}}" style="width: 100%; height: auto;">
                <h6 class="mb-4" style="font-weight: 500;color: #999999;">No orders available at the moment!</h6>
            </div>
        </div>
    </div>
    @else

    <div class="card-group">
        @foreach($orders as $order)

        <div class="card">
            <div class="card-header">
                You have cancelled this order!
            </div>
            <div class="card-body">
                <p class="card-text"><i class="fa fa-user fa-fw me-3 "></i>{{ date('F j, Y h:i:s a', strtotime($order->expected_delivery_date) ); }}</p>
                <p class="card-text"><i class="fa fa-map-marker fa-fw me-3 "></i>{{$order->pick_up}}</p>
                <p class="card-text"><i class="fa fa-location-arrow fa-fw me-3"></i>{{$order->drop_off}}</p>
            </div>

        </div>

        @endforeach
    </div>
    @endif

</div>
<!-- Modal -->

<!-- Modal -->



@endsection


@push('js')
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>

@endpush