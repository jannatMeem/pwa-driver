<!-- FLASH MESSAGE-->
@if ($flash = session('message'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <i class="fa fa-exclamation-circle me-2"></i>{{Session::get('message')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

@endif

@if ($flash = session('dltmsg'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <i class="fa fa-exclamation-circle me-2"></i>{{Session::get('dltmsg')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if ($flash = session('warningmsg'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <i class="fa fa-exclamation-circle me-2"></i>{{Session::get('warningmsg')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if ($flash = session('infomsg'))
<div class="alert alert-info alert-dismissible fade show" role="alert">
    <i class="fa fa-exclamation-circle me-2"></i>{{Session::get('infomsg')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if ($errors->any())
<div class="alert-dismissable" style="margin: 30px 20px;">
    <ul>
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <li>{{ $error }}</li>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>

        @endforeach
    </ul>
</div>
@endif
<!-- FLASH MESSAGE END-->