<div class="nav-item dropdown">
    <!-- <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" onclick="getNotification()"> -->
    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" onclick="getNotification()">
        <i class="fa fa-bell me-lg-2"><span class="badge" id="notiCount" style="color:black;padding: 0; margin-bottom: 15px;"></span></i>
        <span class="d-none d-lg-inline-flex">Notification</span>
    </a>
    <input type="hidden" id="today" value="{{date('Y-m-d H:i:s')}}">
    <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0 ">
        <div id="notification" style=" padding: 10px; border-radius: 4px;">

        </div>
        <!-- <a href="#" class="dropdown-item text-center">See all notifications</a> -->
    </div>
</div>



@push('js')
<script type="text/javascript">
    $(document).ready(function() {


        var delay = 10000; // Delay between GPS update, in milliseconds.
        var timer = null; // Interval timer.


        var init = window.setInterval(function() {
            getNotification();
        }, delay);


    });

    function getNotification() {
        $.ajax({

            type: 'Get',

            url: "{{url('/getNotification')}}",
            dataType: 'json',
            encode: true,

            complete: function(data) {
                var response = data.responseJSON;

                if (response.unseen_notification > 0) {
                    $('#notiCount').html(response.unseen_notification);
                }

                var showNotification = '';
                if (response.notification == 'No') {
                    // console.log('if');

                    showNotification = '<h6 class="fw-normal mb-0">No Notification Found!</h6>';
                    $('#notification').html(showNotification);

                } else {
                    $.each(response.notification, function(key, value) {

                        var title = value.title;
                        var time = value.created_on;
                        time = getTime(time);
                        showNotification += '<a href="{{url("/deliveryPool")}}" class="dropdown-item" style=" margin: 5px;width: 96%;">' +
                            '<h6 class="fw-normal mb-0">A New Delivery Has Arrived</h6>' +
                            '<small>' + time + ' ago</small>' +
                            '</a>' +
                            '<hr class="dropdown-divider">';

                    });
                    $('#notification').html(showNotification);

                }

            }
        });

    }

    function getTime(time) {
        var today = $('#today').val();
        var arriveDay = time;
        var diffMs = (new Date(today) - new Date(arriveDay));
        var diffMins = Math.round(diffMs / 60000);

        if (diffMins < 60) {
            var min = diffMins + "minutes";
            return min;

        } else {

            var diffHrs = Math.floor(diffMins / 60);

            if (diffHrs <= 48) {
                var hour = diffHrs + " hours";
                return hour;
            } else {
                var diffDays = Math.floor(diffHrs / 24);
                var day = diffDays + " days";
                return day;
            }
        }
        // var diffDays = Math.floor(diffMs / 86400000); // days
        // var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        // var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        // console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes until Christmas =)");
    }
</script>
@endpush