@push('css')
<style>
    .dropdown-item {
        /* background: #d2eaff; */
        border: 1px solid #92d5ff;
    }
</style>

@endpush

<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<div class="sidebar pe-4 pb-3">
    <nav class="navbar bg-light navbar-light sidebar-nav">
        <!-- <a href="{{asset('')}}" class="navbar-brand mx-4 mb-3">
        <img class=" me-lg-2" src="{{asset('img/F2N.png')}}" alt="" style="width: 100%; height: 40px">
        </a> -->
        <div class="d-flex align-items-center ms-4 mt-4">


            <div class="position-relative">
                <h6 class="sidebar-header-text">Welcome</h6>

            </div>
        </div>
        <div class="navbar-nav w-100">
            <a href="{{url('/dashboard')}}" class="nav-item nav-link"><i class="fa fa-tachometer-alt me-2"></i>Dashboard</a>
            <a href="{{url('/deliveryPool')}}" class="nav-item nav-link"><i class="fa fa-chart-bar me-2"></i>Delivery Pool</a>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="fa fa-th  me-2"></i>Orders</a>
                <div class="dropdown-menu bg-transparent border-0">
                    <a href="{{url('/pendingOrders')}}" class="dropdown-item "><i class="fa fa-tasks me-2"></i>Pending Orders</a>
                    <a href="{{url('/acceptedOrders')}}" class="dropdown-item"><i class="fa fa-tasks me-2"></i>Accepted Orders</a>
                    <a href="{{url('/pickupOrders')}}" class="dropdown-item"><i class="fa fa-tasks me-2"></i>Picked Up Orders</a>
                    <a href="{{url('/ongoingOrder')}}" class="dropdown-item"><i class="fa fa-tasks me-2"></i>Ongoing Order</a>
                    <!-- <a href="{{url('/completedOrders')}}" class="dropdown-item">Completed Order</a> -->
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"><i class="fa fa-truck me-2"></i>Delivery</a>
                <div class="dropdown-menu bg-transparent border-0">
                    <a href="{{url('/completedOrders')}}" class="dropdown-item"><i class="fa fa-tasks me-2"></i>Completed Delivery</a>
                    <a href="{{url('/cancelledOrders')}}" class="dropdown-item"><i class="fa fa-tasks me-2"></i>Cancelled Delivery</a>

                </div>
            </div>
            <a href="{{url('/profile')}}" class="nav-item nav-link"><i class="fa fa-user me-2"></i>Profile</a>
            <a href="{{url('/settings')}}" class="nav-item nav-link"><i class="fa fa-cog me-2"></i>Settings</a>
            <a href="{{url('/logout')}}" class="nav-item nav-link"><i class="fa fa-sign-out me-2"></i>Logout</a>

        </div>
    </nav>
</div>