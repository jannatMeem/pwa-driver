<div>
    <input id="driverId" value="{{Auth::guard('driver')->user()->drvid}}" type="hidden">
    <input type="hidden" id="dlatitude" name="dlatitude" value="">
    <input type="hidden" id="dlongitude" name="dlongitude" value="">

</div>
@push('js')
<script type="text/javascript">
    $(document).ready(function() {
        var driverId = $('#driverId').val();
        // Rider ID - Fixed to 999 for this demo.
        var delay = 5000; // Delay between GPS update, in milliseconds.
        var timer = null; // Interval timer.
        var display = null;
        var Lat = null;
        var Long = null;

        var init = window.setInterval(function() {
            updateLocation();
        }, delay);


        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(pickSuccess, pickError);
        } else {
            console.log("location not supported");

        }

        function pickSuccess(position) {
            addition = position.coords.latitude + "," + position.coords.longitude;

            Lat = $('#dlatitude').val(position.coords.latitude);
            Long = $('#dlongitude').val(position.coords.longitude);
            updateLocation();

        }


        function pickError(err) {
            console.error(err.message);
            // location.reload();
            // break;
        }

        function updateLocation() {
            $.ajax({

                type: 'Get',

                url: "{{url('/updateLocation')}}/" + driverId,
                dataType: 'json',
                data: 'Lat=' + $('#dlatitude').val() + ' &Long=' + $('#dlongitude').val(),
                encode: true,

                complete: function(data) {
                    // console.log(data.responseJSON);
                }
            });

        }



    });
</script>
@endpush