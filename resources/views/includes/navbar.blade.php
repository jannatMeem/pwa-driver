<nav class="navbar navbar-expand navbar-light sticky-top px-4 py-0" style="">

    <a href="#" class="sidebar-toggler flex-shrink-0">
        <i class="fa fa-bars"></i>
    </a>

    <a href="#" class="navbar-brand d-flex me-4" style="margin: auto;">
        <img class=" me-lg-2" src="{{asset('img/F2N.png')}}" alt="logo" style="width: 100%; height: 40px">
    </a>
    <!-- <form class="d-none d-md-flex ms-4" onsubmit="return setSearch(this)">
        <input class="form-control border-0" id="search" name="search" type="search" placeholder="Search">
        <button class="btn btn-success" type="submit">search</button>
    </form> -->
    <div class="navbar-nav align-items-center ms-auto user-info">


        @include('includes.notification')
        <div class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                <img class="rounded-circle me-lg-2" src="{{asset('')}}{{Auth::guard('driver')->user()->profile_img}}" alt="" style="width: 40px; height: 40px;">
                <span class="d-none d-lg-inline-flex" >{{ Auth::guard('driver')->user()->fname == NULL ? '': Auth::guard('driver')->user()->fname }}{{' '}}{{ Auth::guard('driver')->user()->lname == NULL ? '': Auth::guard('driver')->user()->lname }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                <a href="{{url('/profile')}}" class="dropdown-item"><i class="fa fa-user fa-fw   me-2"></i>My Profile</a>
                <a href="{{url('/settings')}}" class="dropdown-item"><i class="fa fa-cog fa-spin fa-fw me-2"></i>Settings</a>
                <a href="{{url('/logout')}}" class="dropdown-item"><i class="fa fa-arrow-circle-left fa-fw  me-2"></i>Log Out</a>
            </div>
        </div>
    </div>
</nav>

@push('js')
<script>
    function setSearch() {
        var searchValue = $('#search').val();
        $.ajax({
            type: 'GET',
            url: "{{url('/search')}}/" + searchValue,
            dataType: 'json',
            encode: true,

            complete: function(data) {
                var response = data.responseJSON;
                //    console.log(response)

            }

        })
        
    }
</script>

@endpush