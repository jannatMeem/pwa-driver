<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});


// Route::get('/driver/upload/{type}', [DriverController::class,'upload'])->name('driver.upload');





Route::get('/logout', [DriverController::class, 'logout']);


//IF DRIVER IS NOT LOGIN TAKE TO THE LOGIN PAGE
Route::group(['middleware' => 'driverAuth'], function () {
    //DRIVER DASHBOARD
    Route::get('/updateLocation/{id}', [DriverController::class, 'updateLocation']);
    Route::get('/getNotification', [DriverController::class, 'getNotification']);


    Route::get('dashboard', [DriverController::class, 'dashboard'])->name('driver.dashboard');
    Route::get('/getDelivery/{id}', [DeliveryController::class, 'getDelivery']);


    Route::get('/deliveryPool', [DriverController::class, 'deliveryPool'])->name('driver.orders.pool');
    Route::post('/driver/acceptDelivery', [DeliveryController::class, 'acceptDelivery']);


    Route::get('/acceptedOrders', [DriverController::class, 'acceptedOrders'])->name('driver.orders.accepted');

    Route::get('/pickupOrder/{id}', [DeliveryController::class, 'pickOrder']);
    Route::get('/pickupOrders', [DriverController::class, 'pickupOrders']);

    Route::get('/ongoingOrder', [DriverController::class, 'ongoingOrder'])->name('driver.orders.ongoing');
    Route::post('/driver/delivery/status', [DeliveryController::class, 'deliveryStatusChange']);



    Route::get('/pendingOrders', [DriverController::class, 'pendingOrders'])->name('driver.orders.pending');
    Route::post('/driver/deliveryAction/{status}', [DeliveryController::class, 'deliveryAction']);

    Route::get('/completedOrders', [DriverController::class, 'completedOrders'])->name('driver.orders.delivered');
    Route::get('/cancelledOrders', [DriverController::class, 'cancelledOrders'])->name('driver.orders.cancelled');

    Route::get('/profile', function () {
        return view('Driver/driverProfile');
    })->name('driver.profile');
    Route::get('/settings', [DriverController::class, 'profileSettings'])->name('driver.settings');

    Route::post('/driver/driverupdate', [DriverController::class, 'editProfile']);
    Route::post('profile/picture/remove', [DriverController::class, 'pictureRemove']);

    Route::get('/search/{keyword}', [DeliveryController::class, 'search']);
});




Route::group(['middleware' => 'driverLogin'], function () {
    //DRIVER LOGIN
    Route::get('/login', function () {
        return view('Driver/login');
    })->name('driver.login');
    Route::post('/driver/auth/login', [DriverController::class, 'driverAuthenticateLogin'])->name('driver.auth');
    Route::get('register', [DriverController::class,'register'])->name('driver.register');
    Route::post('/driver/create', [DriverController::class,'driverCreate'])->name('driver.create');
});
